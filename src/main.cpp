#include <stdio.h>
#include <windows.h>

#include "TSFuncs.hpp"

const int gMaxTriggerKeys = 5; //This seems to be 5 in Blockland rather than 6 as shown in TGE source

struct Move
{
	int px, py, pz;
	unsigned int pyaw, ppitch, proll;
	float x, y, z;
	float yaw, pitch, roll;
	unsigned int id;
	unsigned int sendCount;

	bool freeLook;
	bool trigger[gMaxTriggerKeys];
};

void moveToString(const Move *const move, char *const buffer, size_t size)
{
	if(!move)
	{
		buffer[0] = '0';
		buffer[1] = '\0';
	}
	else
	{
		unsigned int flags = move->freeLook ? 1 : 0;
		for(int i = 0; i < gMaxTriggerKeys; ++i)
		{
			if(move->trigger[i])
				flags |= 1 << (i + 1);
		}

		snprintf(buffer, size, "%f %f %f %f %f %f %u",
		    move->x, move->y, move->z,
		    move->yaw, move->pitch, move->roll,
		    flags);
	}
}

void stringToMove(Move *const move, const char *const buffer)
{
	unsigned int flags = 0;

	sscanf(buffer, "%f %f %f %f %f %f %u",
	    &move->x, &move->y, &move->z,
	    &move->yaw, &move->pitch, &move->roll,
	    &flags);

	move->freeLook = (flags & 1) != 0;
	for (int i = 0; i < gMaxTriggerKeys; ++i)
		move->trigger[i] = (flags & (1 << (i + 1))) != 0;
}

BlFunctionDef(void, __thiscall, Projectile__processTick, ADDR *, const Move *);
void __fastcall Projectile__processTickHook(ADDR *, void *, const Move *);
BlFunctionHookDef(Projectile__processTick);

void __fastcall Projectile__processTickHook(ADDR *objPtr, void *blank, const Move *move)
{
	ADDR obj = (ADDR)objPtr;
	if(!(*(ADDR *)(obj + 68) & 2))
	{
		char id[16];
		snprintf(id, 16, "%d", *(ADDR *)(obj + 32));

		char buf[512];
		moveToString(move, buf, 512);

		char t_buffer[512];
		size_t t_size = 512;
		snprintf(t_buffer, t_size, "%f %f %f 1 0 0 0",
		    *(float *)(obj + 552),
		    *(float *)(obj + 556),
		    *(float *)(obj + 560)
		);

		char v_buffer[512];
		size_t v_size = 512;
		snprintf(v_buffer, v_size, "%f %f %f",
		    *(float *)(obj + 564),
		    *(float *)(obj + 568),
		    *(float *)(obj + 572)
		);

		tsf_BlCon__executef(5, "onProjectileProcessTick", id, buf, t_buffer, v_buffer);
	}

	Projectile__processTickHookOff();
	Projectile__processTick(objPtr, move);
	Projectile__processTickHookOn();
}

bool init()
{
	BlInit;

	if(!tsf_InitInternal())
		return false;

	//Thanks to StreamShark for the signature
	BlScanFunctionHex(Projectile__processTick, "55 8B EC 83 E4 F8 81 EC ? ? ? ? A1 ? ? ? ? 33 C4 89 84 24 ? ? ? ? 56 57 8B F9 89 7C 24 5C");

	Projectile__processTickHookOn();

	BlPrintf("MoveProjectile: initialized");

	return true;
}

bool deinit()
{
	Projectile__processTickHookOff();

	BlPrintf("MoveProjectile: deinitialized");

	return true;
}

bool __stdcall DllMain(HINSTANCE hinstance, unsigned int reason, void *reserved)
{
	switch(reason)
	{
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return deinit();
		default:
			return true;
	}
}

extern "C" void __declspec(dllexport) __cdecl MoveProjectile(){}
