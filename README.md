# Move Projectile
A DLL that provides a TorqueScript callback for Projectile::processTick. This lets you process every movement of a projectile from script.

This is almost entirely copied from `Move Handler`, with credit to [Port](https://github.com/qoh) for the original version and [Gytyyhgfffff](https://github.com/bansheerubber) for the r2001 version. Credit to [Eagle517](https://gitlab.com/Eagle517/move-handler) for the currently maintained verison from which this was forked.

Credit to StreamShark for finding the signature for me.

## Usage
`onProjectileProcessTick(%projectile, %move, %transform, %velocity)`

Called for every Projectile::processTick call in the engine. It seems to be the case that a projectile cannot have a valid `%move`, but is provided just in case it does.

`%transform` and `%velocity` are provided as a convenience for speed purposes, even though they are already obtainable from script.

## Example

```
function onProjectileProcessTick(%projectile, %move, %transform, %velocity)
{
	echo("Projectile "@ %projectile @" is at: transform={"@ %transform @"} velocity={"@ %velocity @"}");
}
```